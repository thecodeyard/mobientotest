package com.thecodeyard.network;

import com.thecodeyard.network.service.details.DetailsService;
import com.thecodeyard.network.service.search.SearchService;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class that provides convenient methods for instantiating Retrofit services. Currently it supports {@link SearchService} and {@link DetailsService}.
 */
public class ServiceFactory {
	private static final String BASE_URL           = "https://api.500px.com/v1/photos/";
	private static final String PARAM_CONSUMER_KEY = "consumer_key";
	private static final String PARAM_IMAGE_SIZE   = "image_size";
	private static final String VALUE_CONSUMER_KEY = "TpbALPa9VCkzY2YkZhKtQcr2GD9It4UkT8DiMNTp";
	private static final String VALUE_IMAGE_SIZE   = "440";

	private static Retrofit retrofit;

	private static Retrofit getRetrofit() {
		if(retrofit == null){
			final Interceptor headerInterceptor = new Interceptor() {
				@Override
				public Response intercept(Chain chain) throws IOException {
					Request request = chain.request();
					HttpUrl url = request.url()
										 .newBuilder()
										 .addQueryParameter(PARAM_CONSUMER_KEY, VALUE_CONSUMER_KEY)
										 .addQueryParameter(PARAM_IMAGE_SIZE, VALUE_IMAGE_SIZE)
										 .build();
					request = request.newBuilder().url(url).build();

					return chain.proceed(request);
				}
			};

			final OkHttpClient httpClient = new OkHttpClient.Builder()
					.addInterceptor(headerInterceptor)
					.build();

			retrofit = new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.addConverterFactory(GsonConverterFactory.create())
					.client(httpClient)
					.build();
		}

		return retrofit;
	}

	/**
	 * Creates and returns a {@link SearchService}.
	 *
	 * @return the newly created service.
	 */
	public static SearchService getSearchService() {
		return getRetrofit().create(SearchService.class);
	}

	/**
	 * Creates and returns a {@link DetailsService}.
	 *
	 * @return the newly created service.
	 */
	public static DetailsService getDetailsService() {
		return getRetrofit().create(DetailsService.class);
	}
}