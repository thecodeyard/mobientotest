package com.thecodeyard.network.service.search.model;

import com.google.gson.annotations.SerializedName;
import com.thecodeyard.network.service.common.model.User;

import lombok.Data;

@Data
public class Photo {
	@SerializedName("id")
	private long   id;
	@SerializedName("name")
	private String name;
	@SerializedName("image_url")
	private String imageUrl;
	@SerializedName("user")
	private User   user;
}