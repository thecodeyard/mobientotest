package com.thecodeyard.network.service.details.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class DetailsResponse {
	@SerializedName("photo")
	private PhotoDetails photoDetails;
}
