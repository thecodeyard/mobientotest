package com.thecodeyard.network.service.search.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class SearchResponse {
	@SerializedName("current_page")
	private int currentPage;
	@SerializedName("total_pages")
	private int totalPages;
	@SerializedName("photos")
	private ArrayList<Photo> photos;
}
