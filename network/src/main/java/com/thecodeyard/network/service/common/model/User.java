package com.thecodeyard.network.service.common.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class User {
	@SerializedName("firstname")
	private String  firstName;
	@SerializedName("lastname")
	private String  lastName;
	@SerializedName("avatars")
	private Avatars avatars;
}
