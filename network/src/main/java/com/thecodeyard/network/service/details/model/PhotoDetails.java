package com.thecodeyard.network.service.details.model;

import com.google.gson.annotations.SerializedName;
import com.thecodeyard.network.service.common.model.User;

import lombok.Data;

@Data
public class PhotoDetails {
	@SerializedName("name")
	private String name;
	@SerializedName("description")
	private String description;
	@SerializedName("image_url")
	private String imageUrl;
	@SerializedName("favorites_count")
	private int    favoritesCount;
	@SerializedName("user")
	private User   user;
	@SerializedName("times_viewed")
	private long   timesViewed;
}