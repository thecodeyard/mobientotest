package com.thecodeyard.network.service.details;

import com.thecodeyard.network.service.details.model.DetailsResponse;
import com.thecodeyard.network.service.details.model.PhotoDetails;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DetailsService {
	@GET("{id}")
	Call<DetailsResponse> getDetails(@Path("id") long id);
}
