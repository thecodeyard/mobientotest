package com.thecodeyard.network.service.common.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Large {
	@SerializedName("https")
	private String url;
}
