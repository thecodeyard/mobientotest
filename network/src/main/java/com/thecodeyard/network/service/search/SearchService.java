package com.thecodeyard.network.service.search;

import com.thecodeyard.network.service.search.model.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchService {
	@GET("search")
	Call<SearchResponse> search(@Query(value = "term", encoded = true) String query);

	@GET("search")
	Call<SearchResponse> getMoreResults(@Query(value = "term", encoded = true) String query, @Query(value = "page") int page);
}
