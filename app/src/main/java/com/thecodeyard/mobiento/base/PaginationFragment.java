package com.thecodeyard.mobiento.base;

/**
 * A basic {@link ProgressFragment} that wraps functionality necessary for pagination. Classes that extend this fragment are responsible for resetting the pagination {@link #resetPagination()} and
 * updating the current page {@link #setCurrentPage(int)} as well as the total number of pages {@link #setTotalPages(int)}.
 */
public class PaginationFragment extends ProgressFragment {
	private static final int FIRST_PAGE   = 1;
	private static final int INVALID_PAGE = -1;

	private int currentPage = FIRST_PAGE;
	private int totalPages  = FIRST_PAGE;

	/**
	 * Calls {@link #setCurrentPage(int)} and {@link #setTotalPages(int)} and passes 1 to both of them.
	 */
	protected void resetPagination() {
		setCurrentPage(FIRST_PAGE);
		setTotalPages(FIRST_PAGE);
	}

	/**
	 * Updates the total number of pages.
	 *
	 * @param pageCount The new value.
	 */
	protected void setTotalPages(int pageCount) {
		this.totalPages = pageCount;
	}

	/**
	 * Updates the current page number.
	 *
	 * @param currentPage The new value.
	 */
	protected void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * Returns true if there are more pages. False otherwise.
	 *
	 * @return The result.
	 */
	protected boolean hasMorePages() {
		return totalPages > currentPage;
	}

	/**
	 * Returns the next page (current page + 1) if there are more pages. Otherwise, it returns -1.
	 *
	 * @return The result.
	 */
	protected int getNextPage() {
		return hasMorePages() ? this.currentPage + 1 : INVALID_PAGE;
	}
}