package com.thecodeyard.mobiento.base;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import com.thecodeyard.mobiento.R;

import butterknife.BindView;

/**
 * A basic {@link Fragment} that provides methods such as {@link #showProgressBar()}, {@link #hideProgressBar()}, and {@link #isProgressBarShown()}.
 * Subclasses of this fragment are expected to include a {@link ProgressBar} with id "progress_bar".
 */
public class ProgressFragment extends Fragment {

	@BindView(R.id.progress_bar) ProgressBar progressBar;

	protected void showProgressBar() {
		progressBar.setVisibility(View.VISIBLE);
	}

	protected void hideProgressBar() {
		progressBar.setVisibility(View.GONE);
	}

	protected boolean isProgressBarShown(){
		return progressBar.isShown();
	}
}