package com.thecodeyard.mobiento.search;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.thecodeyard.mobiento.R;
import com.thecodeyard.mobiento.base.PaginationFragment;
import com.thecodeyard.mobiento.search.adapter.SearchAdapter;
import com.thecodeyard.mobiento.search.listener.OnScrollEndListener;
import com.thecodeyard.network.ServiceFactory;
import com.thecodeyard.network.service.search.model.SearchResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This Fragment allows the user to type a search query which will be used for fetching data via a {@link com.thecodeyard.network.service.search.SearchService}. The data are displayed in
 * RecyclerView with CardViews.
 */
public class SearchFragment extends PaginationFragment implements Callback<SearchResponse>, OnScrollEndListener.Listener {
	@BindView(R.id.recyclerview) RecyclerView recyclerView;

	private Call<SearchResponse> searchCall;
	private String               searchTerm;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_search, container, false);
		ButterKnife.bind(this, view);

		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setAdapter(new SearchAdapter());
		recyclerView.addOnScrollListener(new OnScrollEndListener(layoutManager, this));

		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_search, menu);

		final MenuItem searchMenuItem = menu.findItem(R.id.action_search);

		final SearchView searchView = (SearchView) searchMenuItem.getActionView();
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				if (!searchView.isIconified()) {
					searchView.setIconified(true);
				}

				searchMenuItem.collapseActionView();

				// Keep the search query in a class field. It will be used for pagination.
				searchTerm = query;

				// Clear existing data from adapter.
				SearchAdapter adapter = (SearchAdapter) recyclerView.getAdapter();
				adapter.clear();
				adapter.notifyDataSetChanged();

				// For convenience we'll leave trigger the search from here instead of setting a time delay and putting it inside the onQueryTextChange().
				searchCall = ServiceFactory.getSearchService().search(query);
				searchCall.enqueue(SearchFragment.this);
				resetPagination();
				showProgressBar();
				return true;
			}

			@Override
			public boolean onQueryTextChange(String s) {
				return false;
			}
		});
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
		final SearchResponse searchResponse = response.body();
		SearchAdapter adapter = (SearchAdapter) recyclerView.getAdapter();
		adapter.addAll(searchResponse.getPhotos());
		adapter.notifyDataSetChanged();

		setCurrentPage(searchResponse.getCurrentPage());
		setTotalPages(searchResponse.getTotalPages());

		hideProgressBar();
	}

	@Override
	public void onFailure(Call<SearchResponse> call, Throwable t) {
		hideProgressBar();
		t.printStackTrace();
	}

	@Override
	public void onEndReached() {
		// Fetch the next page if there are more and there are no pending requests (progress bar is shown).
		if (hasMorePages() && !isProgressBarShown()) {
			searchCall = ServiceFactory.getSearchService().getMoreResults(searchTerm, getNextPage());
			searchCall.enqueue(SearchFragment.this);
			showProgressBar();
		}
	}
}