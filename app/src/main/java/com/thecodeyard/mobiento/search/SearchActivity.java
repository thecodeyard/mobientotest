package com.thecodeyard.mobiento.search;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.thecodeyard.mobiento.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A minimalistic activity that simply displays a {@link SearchFragment}.
 */
public class SearchActivity extends AppCompatActivity {
	@BindView(R.id.toolbar) Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		ButterKnife.bind(this);

		setSupportActionBar(toolbar);
	}
}