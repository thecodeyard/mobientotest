package com.thecodeyard.mobiento.search.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thecodeyard.mobiento.R;
import com.thecodeyard.mobiento.details.DetailsActivity;
import com.thecodeyard.network.service.common.model.User;
import com.thecodeyard.network.service.search.model.Photo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Simple adapter for filling a recycler view with search results.
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
	private ArrayList<Photo> photos;

	public SearchAdapter() {
		photos = new ArrayList<>();
	}

	/**
	 * Adds the specified photos to the bottom of the existing list.
	 *
	 * @param photos The list of photos to be added.
	 */
	public void addAll(ArrayList<Photo> photos) {
		this.photos.addAll(photos);
	}

	/**
	 * Clears the array of photos.
	 */
	public void clear() {
		photos = new ArrayList<>();
	}

	@Override
	public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cardview, parent, false);
		return new SearchViewHolder(view);
	}

	@Override
	public void onBindViewHolder(SearchViewHolder holder, int position) {
		final Photo photo = photos.get(position);
		final User user = photo.getUser();

		holder.title.setText(photo.getName());
		holder.user.setText(String.format(holder.user.getContext().getString(R.string.by_user), user.getFirstName(), user.getLastName()));

		Glide.with(holder.image.getContext())
			 .load(photo.getImageUrl())
			 .centerCrop()
			 .crossFade()
			 .into(holder.image);

		Glide.with(holder.avatar.getContext())
			 .load(user.getAvatars().getLarge().getUrl())
			 .centerCrop()
			 .crossFade()
			 .into(holder.avatar);
	}

	@Override
	public int getItemCount() {
		return photos.size();
	}

	class SearchViewHolder extends RecyclerView.ViewHolder {
		@BindView(R.id.image)  ImageView image;
		@BindView(R.id.avatar) ImageView avatar;
		@BindView(R.id.title)  TextView  title;
		@BindView(R.id.user)   TextView  user;

		SearchViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}

		@OnClick(R.id.cardview)
		@SuppressWarnings("unused")
		public void onClick(View v) {
			final Photo photo = photos.get(getAdapterPosition());
			final Context context = v.getContext();
			final Intent intent = new Intent(context, DetailsActivity.class);
			intent.putExtra(DetailsActivity.EXTRA_PHOTO_ID, photo.getId());
			context.startActivity(intent);
		}
	}
}
