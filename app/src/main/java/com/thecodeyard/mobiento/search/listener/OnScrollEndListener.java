package com.thecodeyard.mobiento.search.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.lang.ref.WeakReference;

public class OnScrollEndListener extends RecyclerView.OnScrollListener {

	private WeakReference<LinearLayoutManager> layoutManagerWeakReference;
	private Listener listener;

	public OnScrollEndListener(LinearLayoutManager layoutManager, Listener listener) {
		layoutManagerWeakReference = new WeakReference<>(layoutManager);
		this.listener = listener;
	}

	@Override
	public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
		super.onScrolled(recyclerView, dx, dy);

		final LinearLayoutManager layoutManager = layoutManagerWeakReference.get();

		if (layoutManager != null) {
			final int visibleItemCount = layoutManager.getChildCount();
			final int totalItemCount = layoutManager.getItemCount();
			final int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

			if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
				this.listener.onEndReached();
			}
		}
	}

	public interface Listener {
		void onEndReached();
	}
}