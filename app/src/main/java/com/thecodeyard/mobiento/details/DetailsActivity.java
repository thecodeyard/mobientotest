package com.thecodeyard.mobiento.details;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.thecodeyard.mobiento.R;

/**
 * This activity receives a photo id in its extras and passes it to its {@link DetailsFragment}.
 */
public class DetailsActivity extends AppCompatActivity {
	public static final String EXTRA_PHOTO_ID = "EXTRA_PHOTO_ID";
	public static final int DEFAULT_VALUE = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);

		long photoId = getIntent().getLongExtra(EXTRA_PHOTO_ID, DEFAULT_VALUE);
		getSupportFragmentManager().beginTransaction().add(R.id.fragment_holder,DetailsFragment.newInstance(photoId)).commit();
	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return true;
	}
}