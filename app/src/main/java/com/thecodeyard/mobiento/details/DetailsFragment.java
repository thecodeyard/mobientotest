package com.thecodeyard.mobiento.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thecodeyard.mobiento.R;
import com.thecodeyard.mobiento.base.ProgressFragment;
import com.thecodeyard.network.ServiceFactory;
import com.thecodeyard.network.service.common.model.User;
import com.thecodeyard.network.service.details.model.DetailsResponse;
import com.thecodeyard.network.service.details.model.PhotoDetails;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Receives a photo id and fetches the corresponding data via {@link com.thecodeyard.network.service.details.DetailsService}. The data are displayed using a {@link android.support.design.widget.CollapsingToolbarLayout}
 * and a {@link android.support.v4.widget.NestedScrollView}.
 */
public class DetailsFragment extends ProgressFragment implements Callback<DetailsResponse> {
	private static final String EXTRA_PHOTO_ID = "EXTRA_PHOTO_ID";

	@BindView(R.id.image)        ImageView imageView;
	@BindView(R.id.avatar)       ImageView avatarView;
	@BindView(R.id.user)         TextView  userTextView;
	@BindView(R.id.times_viewed) TextView  viewsTextView;
	@BindView(R.id.title)        TextView  titleTextView;
	@BindView(R.id.description)  TextView  descriptionTextView;
	@BindView(R.id.toolbar)      Toolbar   toolbar;

	private Call<DetailsResponse> call;

	public static DetailsFragment newInstance(long photoId) {
		DetailsFragment fragment = new DetailsFragment();

		Bundle args = new Bundle();
		args.putLong(EXTRA_PHOTO_ID, photoId);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		AppCompatActivity activity = (AppCompatActivity) getActivity();
		activity.setSupportActionBar(toolbar);

		ActionBar actionBar = activity.getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_details, container, false);
		ButterKnife.bind(this, view);

		showProgressBar();

		long photoId = getArguments().getLong(EXTRA_PHOTO_ID);
		call = ServiceFactory.getDetailsService().getDetails(photoId);
		call.enqueue(this);

		return view;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// Cancel any pending calls when the fragment is destroyed.
		if(call != null && call.isExecuted()){
			call.cancel();
		}
	}

	@Override
	public void onResponse(Call<DetailsResponse> call, Response<DetailsResponse> response) {
		hideProgressBar();
		updateContent(response.body());
	}

	@Override
	public void onFailure(Call<DetailsResponse> call, Throwable t) {
		hideProgressBar();
	}

	private void updateContent(DetailsResponse response) {
		final Context context = getContext();
		final PhotoDetails photo = response.getPhotoDetails();
		final User user = photo.getUser();

		Glide.with(context)
			 .load(photo.getImageUrl())
			 .placeholder(R.drawable.placeholder)
			 .centerCrop()
			 .crossFade()
			 .into(imageView);

		Glide.with(context)
			 .load(user.getAvatars().getLarge().getUrl())
			 .centerCrop()
			 .crossFade()
			 .into(avatarView);

		userTextView.setText(String.format(getString(R.string.user), user.getFirstName(), user.getLastName()));
		viewsTextView.setText(String.format(getString(R.string.times_viewed), photo.getTimesViewed()));
		titleTextView.setText(photo.getName());
		descriptionTextView.setText(photo.getDescription());
	}
}