# Description #
This is a test project for Mobiento. Thank you in advance for taking the time to review this code.

The description suggests that we connect to https://developers.500px.com for fetching the search results. However, JSON response returned by a call similar to this: https://developers.500px.com/search?q=android includes a security chunk (while(1)) that would need to be stripped out. Therefore, I decided to use https://api.500px.com/v1/photos/ instead.

The project consists of two modules: **app** and **network**. The first includes all the UI-related code (as usual), while the latter includes only Retrofit-related code (Services, Models, etc).

The app starts from the SearchActivity/SearchFragment, where the user can input some text and press *submit* in order to trigger the search. The results are presented in a RecyclerView with
CardViews. Pagination is also supported.

Clicking on a CardView launches the DetailsActivity/DetailsFragment and passes the photo id to them. This id is used for fetching more details about this specific photo. The details are presented
using a CollapsingToolbar and a NestedScrollview.

# Simplifications #
For the shake of simplicity, some features were left out of the scope of this project:

* No Hungarian notation is being used. Although Google encourages its usage, there's a lot of debate of whether it's really needed nowadays while in our latest projects we did not use it either. Therefore, it came natural to me to avoid it.
* No automated tests were written.
* Orientation changes are not handled properly. That is, the current instances are not saved before and retrieved after the orientation has changed..
* No special layouts are provided for tablets.
* The image_size url parameter is set once for all calls in the ServiceFactory.
* No empty view is shown when there are no results to display.

# 3rd Party Libraries #
This project makes use of the following 3rd party libraries:

* Retrofit 2
* Gson
* Lombok
* Glide
* Butterknife
* CircleImageView

# Contact #
georgios.savvidis@thecodeyard.com